"""Core API Views"""
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from core.serializers import AnsibleInventorySerializer, TaskSerializer


class LoadFactsView(APIView):
    """Load server facts"""

    # Required for the Browsable API renderer to have a nice form.
    serializer_class = TaskSerializer

    def post(self, request):
        """Post method"""

        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetAnsibleInventoryView(APIView):
    """Get Ansible inventory"""

    # Required for the Browsable API renderer to have a nice form.
    serializer_class = AnsibleInventorySerializer

    def get(self, request):
        """Get method"""

        serializer = AnsibleInventorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
