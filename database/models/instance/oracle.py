"""Oracle Instance models"""

from django.db import models
from django.urls import reverse
from database.models.instance import Instance
from database.models import DatabaseType


class OracleInstance(Instance):
    """Oracle instance"""

    ASM = 'ASM'
    DATABASE = 'DB'

    ORACLE_INSTANCE_TYPE = [
        (ASM, 'ASM instance'),
        (DATABASE, 'Database instance'),
    ]

    instance_type = models.CharField(max_length=3, choices=ORACLE_INSTANCE_TYPE)

    def save(self, *args, **kwargs):
        self.type = DatabaseType.ORACLE
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        """Return object absolute url"""
        if self.instance_type == self.DATABASE:
            return self.databaseoracleinstance.get_absolute_url()  # pylint: disable=no-member
        else:
            return self.asmoracleinstance.get_absolute_url()  # pylint: disable=no-member

    class Meta:
        db_table = 'database_instance_oracle'


class DatabaseOracleInstance(OracleInstance):
    """Oracle database instance"""

    def save(self, *args, **kwargs):
        self.instance_type = OracleInstance.DATABASE
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.name} on {self.server}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:instance-oracle-database-detail', kwargs={'pk': self.pk})

    class Meta:
        db_table = 'database_instance_oracle_database'


class AsmOracleInstance(OracleInstance):
    """Oracle ASM instance"""

    def save(self, *args, **kwargs):
        self.instance_type = OracleInstance.ASM
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.name} on {self.server}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:instance-oracle-asm-detail', kwargs={'pk': self.pk})

    class Meta:
        db_table = 'database_instance_oracle_asm'
