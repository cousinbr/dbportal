"""Tns base models"""

from django.db import models
from django.urls import reverse
from database.models.database import Database
from database.models.engine import Engine

from system.models import Cluster, Server

# class Listener(models.Model):
#     """Listener model"""

#     name = models.CharField('Name', max_length=30)
#     type = models.CharField(max_length=3, choices=DatabaseType.TYPE)
#     engine = models.ForeignKey(Engine, on_delete=models.CASCADE)

#     class Meta:
#         db_table = 'database_listener'
#         ordering = ['name']

#     def __str__(self):
#         """Return string representation"""
#         return f"{self.name}"

#     def get_absolute_url(self):
#         """Return object absolute url"""

#         if self.type == DatabaseType.ORACLE:
#             return self.oraclelistener.get_absolute_url()  # pylint: disable=no-member

class Listener(models.Model):
    """Listener model"""

    LOCAL = 'LOCAL'
    SCAN = 'SCAN'

    ORACLE_LISTENER_TYPE = [
        (LOCAL, 'Local Listener'),
        (SCAN, 'Scan Listener'),
    ]

    name = models.CharField('Name', max_length=30)
    type = models.CharField(max_length=5, choices=ORACLE_LISTENER_TYPE)
    engine = models.ForeignKey(Engine, on_delete=models.CASCADE)

    class Meta:
        db_table = 'database_listener'

    def __str__(self):
        """Return string representation"""
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""

        if self.type == self.LOCAL:
            return self.locallistener.get_absolute_url()  # pylint: disable=no-member
        else:
            return self.scanlistener.get_absolute_url()  # pylint: disable=no-member


class LocalListener(Listener):
    """LocalListener model"""

    server = models.ForeignKey(Server, on_delete=models.CASCADE)

    class Meta:
        db_table = 'database_listener_local'

    def save(self, *args, **kwargs):
        self.type = Listener.LOCAL
        super().save(*args, **kwargs)

    def __str__(self):
        """Return string representation"""
        return f"{self.name} (Local Listener)"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:listener-local-detail', kwargs={'pk': self.pk})


class ScanListener(Listener):
    """ScanListener model"""

    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE)

    class Meta:
        db_table = 'database_listener_scan'

    def save(self, *args, **kwargs):
        self.type = Listener.SCAN
        super().save(*args, **kwargs)

    def __str__(self):
        """Return string representation"""
        return f"{self.name} (SCAN Listener)"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:listener-scan-detail', kwargs={'pk': self.pk})


class Endpoint(models.Model):
    """Endpoint model"""

    hostname = models.CharField('Hostname', max_length=255)
    port = models.PositiveSmallIntegerField('Port')

    listener = models.ForeignKey(Listener, on_delete=models.CASCADE)

    class Meta:
        db_table = 'database_endpoint'


# pylint: disable=R0903
class ServiceManager(models.Manager):
    """Service manager"""

    def search(self, query=None):
        """Search for service"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = (
                models.Q(name__icontains=query)
            )
            # distinct() is often necessary with Q lookups
            qs = qs.filter(or_lookup).order_by('name')
        return qs


class Service(models.Model):
    """Service Model"""

    name = models.CharField('Name', max_length=512)

    listeners = models.ManyToManyField(Listener)
    database = models.ForeignKey(Database, on_delete=models.CASCADE)

    objects = ServiceManager()

    class Meta:
        db_table = 'database_service'

    def __str__(self):
        """Return string representation"""
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:service-detail', kwargs={'pk': self.pk})
