"""Zdlra module models Test"""
import datetime

from django.test import TestCase

from core.models import Snapshot
from database.models.database.oracle import OracleDatabase
from zdlra.models import Zdlra, ZdlraProtectionPolicy, ZdlraDatabase


class ZdlraTestCase(TestCase):
    """Server model test"""

    name = "ZDLRA"

    def setUp(self):
        snapshot = Snapshot.objects.create(payload="{}")
        Zdlra.objects.create(
            name=self.name,
            snapshot=snapshot
        )

    def test_str(self):
        """InstanceBinary string representation"""
        obj = Zdlra.objects.get(name=self.name)
        self.assertEqual(str(obj), f"{self.name}")

    def test_get_absolute_url(self):
        """InstanceBinary get_absolute_url"""
        obj = Zdlra.objects.get(name=self.name)
        self.assertEqual(obj.get_absolute_url(), "/zdlra/1/")


class ZdlraProtectionPolicyTestCase(TestCase):
    """ZdlraProtectionPolicy model test"""

    zdlra = None
    name = "Protection Policy"
    recovery_window_goal = datetime.timedelta(days=-1, seconds=68400)

    def setUp(self):
        snapshot = Snapshot.objects.create(payload="{}")
        self.zdlra = Zdlra.objects.create(
            name="ZDLRA",
            snapshot=snapshot
        )

        ZdlraProtectionPolicy.objects.create(
            zdlra=self.zdlra,
            name=self.name,
            recovery_window_goal=self.recovery_window_goal
        )

    def test_str(self):
        """InstanceBinary string representation"""
        obj = ZdlraProtectionPolicy.objects.get(name=self.name)
        self.assertEqual(str(obj), f"{self.name} ({self.recovery_window_goal}) on {self.zdlra}")

    def test_get_absolute_url(self):
        """InstanceBinary get_absolute_url"""
        obj = ZdlraProtectionPolicy.objects.get(name=self.name)
        self.assertEqual(obj.get_absolute_url(), "/zdlra/protection-policie/1/")


class ZdlraDatabaseTestCase(TestCase):
    """ZdlraDatabase model test"""

    unique_name = "ORCL"
    zdlra = None
    database = None

    def setUp(self):
        snapshot = Snapshot.objects.create(payload="{}")
        self.zdlra = Zdlra.objects.create(
            name="ZDLRA",
            snapshot=snapshot
        )
        protection_policy = ZdlraProtectionPolicy.objects.create(
            zdlra=self.zdlra,
            name="Protection policy",
            recovery_window_goal=datetime.timedelta(days=-1, seconds=68400)
        )
        self.database = OracleDatabase.objects.create(
            name=self.unique_name,
            dbid=1,
            cluster_id="AA",
            snapshot=snapshot,
            created="2011-09-01T13:20:30+03:00"
        )
        ZdlraDatabase.objects.create(
            state="",
            creation_time="2011-09-01T13:20:30+03:00",
            nzdl_active="YES",
            zdlra=self.zdlra,
            protection_policy=protection_policy,
            snapshot=snapshot,
            database=self.database
        )

    def test_str(self):
        """ZdlraDatabase string representation"""
        obj = ZdlraDatabase.objects.get(zdlra=self.zdlra, database=self.database)
        self.assertEqual(str(obj), f"{self.unique_name} ({self.database.get_type_display()})")

    def test_get_absolute_url(self):
        """ZdlraDatabase get_absolute_url"""
        obj = ZdlraDatabase.objects.get(database=self.database)
        self.assertEqual(obj.get_absolute_url(), "/zdlra/database/1/")
