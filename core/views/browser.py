"""Core brownser views"""

from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, ListView, DetailView

from core.models import Snapshot


class IndexView(LoginRequiredMixin, TemplateView):
    """Core index viex"""

    template_name = "core/index.html"


class LoginView(LoginView):
    """Core login view"""


class LogoutView(LoginRequiredMixin, LogoutView):
    """Core logout view"""

    next_page = "core:index"


class SnapshotListView(LoginRequiredMixin, ListView):
    """Snapshot list view"""

    model = Snapshot
    paginate_by = 10


class SnapshotDetailView(LoginRequiredMixin, DetailView):
    """Snapshot detail view"""

    model = Snapshot
