"""Class to manage TNS aliases in OUD.

Created on 2023.06.12

The goal is to support the creation and manipulation of TNSNAMEs entries.
There are 2 types of entry :
- The standard entries that carry the description, named service.
- Aliases that point to entries of type service.
"""
import logging
from typing import List

from ldap3 import (ALL, DEREF_NEVER, Connection, ObjectDef, Reader, Server,
                   Writer)
from ldap3.core.exceptions import (LDAPBindError, LDAPCursorError,
                                   LDAPExceptionError, LDAPInvalidDnError,
                                   LDAPSocketOpenError)

logger = logging.getLogger(__name__)

class CFMReader(Reader):
    """
    I have no idea why the dereference_aliases parameter is forced
    to DEREF_ALWAYS.
    For our use this is a issue because we want to manage aliases.
    While waiting for this parameter to be an input parameter of
    the Reader class, I propose this local fix.
    """

    def __init__(self, *args, **kwargs):
        """Replace DEREF_ALWAYS by DEREF_NEVER."""
        self.args = args
        self.kwargs = kwargs
        Reader.__init__(self, *self.args, **self.kwargs)
        self.dereference_aliases = DEREF_NEVER

    def clear(self):
        """Replace DEREF_ALWAYS by DEREF_NEVER."""
        Reader.clear(self)
        self.dereference_aliases = DEREF_NEVER


class OudTns():
    """Class to manage TNSNAME entries from OUD.

    On calling the class, a connection will be established with
    the OUD platform. Then the functions will use this connection
    to perform the expected operations.
    """

    server_parameters: dict = {'host': "", 'port': None, 'use_ssl': True, 'get_info': ALL}

    def __init__(self, servers: List[str], port: int, user: str, password: str, base_dn: str) -> None:
        """init.

        First step connect to the OUD platform.
        Second step retrieve the definition of the classes
        of objects to be manipulated.
        """
        self.servers: list = servers
        self.server_parameters['port'] = port
        self.connection_parameters: dict = {'user': user,
                                           'password': password,
                                           'authentication': 'SIMPLE',
                                           'auto_bind': True}
        self.base_dn = base_dn
        self.ldapserver = None
        self.connection = None
        self._connect()
        self.orclnetservice = None
        self.serviceparameters = None
        self.orclnetalias = None
        self.aliasparameters = None
        self._defobject()

    def _defobject(self) -> None:
        """Retrieve definition object class.

        Object class : orclNetService & orclNetServiceAlias.
        The object class definition is appended to the LDAP query
        to specify the targeted object.
        """
        self.orclnetservice = ObjectDef(object_class=['orclNetService'],
                                        schema=self.connection)
        self.serviceparameters = {'connection': self.connection,
                                  'object_def': self.orclnetservice,
                                  'base': self.base_dn,
                                  'sub_tree': True}
        self.orclnetalias = ObjectDef(object_class=['orclNetServiceAlias'],
                                      schema=self.connection)
        self.aliasparameters = {'connection': self.connection,
                                'object_def': self.orclnetalias,
                                'base': self.base_dn,
                                'sub_tree': True}

    def _connect(self) -> None:
        """Connect to the OUD platform.

        Our setup has multiple OUD servers in replication mode.
        To avoid replication conflicts we always write to the same
        available server; We try to connect to the first server
        and if it doesn't go to the next one and so on.
        If no server is available, an exception is raised.
        """
        result = False
        for server in self.servers:
            self.server_parameters['host'] = server
            self.ldapserver = Server(**self.server_parameters)
            logger.info('Try to connect to %s', server)
            try:
                self.connection = Connection(server=self.ldapserver,
                                             **self.connection_parameters)
            except LDAPSocketOpenError as error:
                logger.error('Unable to connect to %s', server)
                logger.error('Server error : %s', error)
                continue
            except LDAPBindError as error:
                logger.error('Unable to connect to %s', server)
                logger.error('Bind error : %s', error)
                continue
            except LDAPExceptionError as error:
                logger.error('Unable to connect to %s', server)
                logger.error('Unknow error : %s', error)
                continue
            result = True
            logger.info('Connected to %s', server)
            break
        if not result:
            raise RuntimeError('No server available')

    def _resul2dict(self, ldapentries: list) -> dict:
        """Convert result Reader Class to a neested dictionary.

        Args:
            ldapentries: result search.

        Returns:
            entries (dict):
                id (str): Entry TNSNAME.
                dn (str): DN entry from OUD.
                attributes (list): all entry attributs.
                tuple (attr name, value).
        """
        ldapentriesdict: dict = {}
        for entry in ldapentries:
            entrydict: dict = {'dn': entry.entry_dn, 'attributes': entry.entry_attributes}
            for attr in entry.entry_attributes:
                entrydict[attr] = entry[attr].value
            ldapentriesdict[entry.cn.value] = entrydict
        return ldapentriesdict

    def _removeentry(self, distinguishedname: str) -> bool:
        """Remove a service or an alias entry using the DN.

        Args:
            tnsname (str): wanted TNSNAME.

        Returns:
            result (bool): True if ok else False.
        """
        try:
            self.connection.delete(dn=distinguishedname)
        except LDAPInvalidDnError as error:
            logger.error('Unable to remove DN %s', distinguishedname)
            logger.error('Message %s', error)
            return False
        logger.info('Removed entry DN: %s', distinguishedname)
        return True

    def close(self) -> None:
        """Close connection."""
        return self.connection.unbind()

    def getservices(self, tnsname: str = None) -> dict:
        """Get services matching a tnsname or all if set to None.

        Tne tnsname is use in search LDAP on cn attribut
        using LDAP expression syntax.
        Args:
            tnsname (str): wanted lettering.

        Returns:
            entries (dict): list of matching entries, empty if nothing foud.
        """
        data: dict = {}
        if tnsname is not None:
            query = '(cn=' + tnsname + ')'
            request = CFMReader(**self.serviceparameters, query=query)
        else:
            request = CFMReader(**self.serviceparameters)
        request.search()
        data = self._resul2dict(request)
        return data

    def getservice(self, tnsname: str = None,
                   distinguishedname: str = None) -> dict:
        """Get service using tnsname or DN.

        Search for a service by TNSNAME or DN.
        If TNSMANE, convert to DN in first.
        Args:
            tnsname (str): wanted TNSNAME.
            distinguishedname (str): wanted DN.

        Returns:
            entry (dict): entry found, empty if not.
        """
        data: dict = {}
        if tnsname is None and distinguishedname is None:
            return data
        if tnsname is not None and distinguishedname is not None:
            return data
        if tnsname is not None:
            distinguishedname = 'cn=' + tnsname + ',' + self.base_dn
        request = CFMReader(**self.serviceparameters)
        request.search_object(entry_dn=distinguishedname)
        data = self._resul2dict(request)
        return data

    def getaliases(self, tnsname: str = None) -> dict:
        """Get aliases matching tnsname or all if set to None.

        Tne tnsname is use in search LDAP on cn attribut
        using LDAP expression syntax.
        Args:
            tnsname (str): wanted lettering.

        Returns:
            entries (dict): list of matching entries, empty if nothing foud.
        """
        data: dict = {}
        if tnsname is not None:
            query = '(cn=' + tnsname + ')'
            request = CFMReader(**self.aliasparameters, query=query)
        else:
            request = CFMReader(**self.aliasparameters)
        request.search()
        data = self._resul2dict(request)
        return data

    def getalias(self, tnsname: str = None,
                 distinguishedname: str = None) -> dict:
        """Get alias using tnsname or DN.

        Search for an alias by TNSNAME or DN.
        If TNSMANE, convert to DN in first.
        Args:
            tnsname (str): wanted TNSNAME.
            distinguishedname (str): wanted DN.

        Returns:
            entry (dict): entry found, empty if not.
        """
        data: dict = {}
        if tnsname is None and distinguishedname is None:
            return data
        if tnsname is not None and distinguishedname is not None:
            return data
        if tnsname is not None:
            distinguishedname = 'cn=' + tnsname + ',' + self.base_dn
        request = CFMReader(**self.aliasparameters)
        request.search_object(entry_dn=distinguishedname)
        data = self._resul2dict(request)
        return data

    def getservicealiases(self, tnsname: str) -> dict:
        """Get all aliases pointing to a given service.

        Generate the DN from the TNSNAME
        then do a search on the aliasedObjectName attribute.
        Args:
            tnsname (str): wanted TNSNAME.

        Returns:
            entries (dict): list of aliases, empty if nothing foud.
        """
        data: dict = {}
        service = self.getservice(tnsname=tnsname)
        if not service:
            return data
        query = '(aliasedObjectName=' + service[tnsname]['dn'] + ')'
        request = CFMReader(**self.aliasparameters, query=query)
        request.search()
        data = self._resul2dict(request)
        return data

    def getdefunctaliases(self) -> dict:
        """Gat all orphan aliases (ie pointing anywhere).

        Args:
            None.

        Returns:
            entries (dict): list of aliases, empty if nothing foud.
        """
        defunctaliases: dict = {}
        allaliases = self.getaliases()
        for tns, alias in allaliases.items():
            servicednused = alias['aliasedObjectName']
            service = self.getservice(distinguishedname=servicednused)
            if not service:
                defunctaliases[tns] = alias
        return defunctaliases

    def addservice(self, tnsname: str, descstring: str,
                   description: str = None) -> bool:
        """Add a new service entry (ie TNS entry).

        Generate the DN from the tnsname of the service
        then create the entry.
        Args:
            tnsname (str): TNSNAME to be created.
            descstring (str): Definition of the TNSNAME.
            description (str): Comment if need.

        Returns:
            result (bool): True if ok else False.
        """
        request = Writer(connection=self.connection,
                         object_def=self.orclnetservice)
        distinguishedname = 'cn=' + tnsname + ',' + self.base_dn
        try:
            entry = request.new(distinguishedname)
        except LDAPCursorError as error:
            logger.error('Unable to create DN %s', distinguishedname)
            logger.error('Cursor error : %s', error)
            return False
        entry.orclNetDescString = descstring
        if description is not None:
            entry.Description = description
        result = request.commit()
        if not result:
            logger.error('Unable to add DN %s', distinguishedname)
            return False
        logger.info('Added entry DN: %s', distinguishedname)
        return True

    def modifyservice(self, tnsname: str, description: str = None,
                      descstring: str = None) -> bool:
        """Modify a service entry.

        Only attribute orclNetDescString and/or Description.
        First check is entry exist as service (ie not an alias).
        Args:
            tnsname (str): TNSNAME to be modified.
            descstring (str): Definition of the TNSNAME.
            description (str): Comment.

        Returns:
            result (bool): True if ok else False.
        """
        if description is None and descstring is None:
            logger.warning('Missing parameter description or descstring')
            return False
        service = self.getservice(tnsname=tnsname)
        if not service:
            logger.warning('Service %s does not exist', tnsname)
            return False
        request = CFMReader(**self.serviceparameters)
        request.search_object(entry_dn=service[tnsname]['dn'])
        entry = Writer.from_cursor(request)
        if description is not None:
            entry[0].Description = description
        if descstring is not None:
            entry[0].orclNetDescString = descstring
        result = entry.commit()
        if not result:
            logger.error('Unable to modify DN %s', service[tnsname]['dn'])
            return False
        logger.info('Modified entry DN: %s', service[tnsname]['dn'])
        return True

    def removeservice(self, tnsname: str, removealiases: bool = False) -> bool:
        """Remove a service entry (ie TNS entry).

        Check if it's a service then remove.
        Args:
            tnsname (str): TNSNAME to be deleted.
            removealiases (bool): True to delete all aliases pointing
            to this service.

        Returns:
            result (bool): True if ok else False.
        """
        service = self.getservice(tnsname=tnsname)
        if not service:
            return False
        if removealiases:
            logger.info('Remove aliases requested')
            aliases = self.getservicealiases(tnsname=tnsname)
            for dummy, alias in aliases.items():
                result = self._removeentry(distinguishedname=alias['dn'])
                if not result:
                    return False
        result = self._removeentry(distinguishedname=service[tnsname]['dn'])
        return result

    def addalias(self, tnsname: str, aliasedservice: str) -> bool:
        """Add a new alias on a specified service.

        First check if the service entry exists.
        Args:
            tnsname (str): TNSNAME to be created.
            aliasedservice (str): TNSNAME of the service to alias.

        Returns:
            result (bool): True if ok else False.
        """
        service = self.getservice(tnsname=aliasedservice)
        if not service:
            logger.error('Service %s not found', aliasedservice)
            return False
        request = Writer(connection=self.connection,
                         object_def=self.orclnetalias)
        distinguishedname = 'cn=' + tnsname + ',' + self.base_dn
        try:
            entry = request.new(distinguishedname)
        except LDAPCursorError as error:
            logger.error('Unable to add DN %s', distinguishedname)
            logger.error('Cursor error : %s', error)
            return False
        entry.aliasedObjectName = service[aliasedservice]['dn']
        result = request.commit()
        if not result:
            logger.error('Unable to add DN %s', distinguishedname)
            return False
        logger.info('Added entry DN: %s', distinguishedname)
        return True

    def removealias(self, tnsname: str) -> bool:
        """Remove a alias entry.

        Check if it's a alias then remove.
        Args:
            tnsname (str): TNSNAME to be deleted.

        Returns:
            result (bool): True if ok else False.
        """
        alias = self.getalias(tnsname=tnsname)
        if not alias:
            return False
        result = self._removeentry(distinguishedname=alias[tnsname]['dn'])
        return result
