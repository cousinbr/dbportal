"""Database browser views"""

from django.db.models import FilteredRelation, Q, F
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView

from csv_export.views import CSVExportView

from database.models.database import Database
from database.models.database.oracle import ContainerOracleDatabase, OracleDatabase, PluggableOracleDatabase
from database.models.diskgroup.oracle import OracleDiskgroup
from database.models.engine import Engine
from database.models.instance import Instance, InstanceParameterMetric
from database.models.instance.oracle import AsmOracleInstance, DatabaseOracleInstance
from database.models.tablespace import Tablespace
from database.models.tablespace.oracle import OracleTablespace
from database.models.tns import LocalListener, ScanListener, Service
from database.models.user import User
from database.models.user.oracle import OracleUser

class InstanceListView(LoginRequiredMixin, ListView):
    """Instance list view"""

    model = Instance
    paginate_by = 10
    queryset = Instance.objects.select_related()


class InstanceExportView(CSVExportView):
    """Instance export view"""

    model = Instance
    fields = "__all__"


class InstanceDetailView(LoginRequiredMixin, DetailView):
    """Instance detail view"""

    model = Instance

    def get_context_data(self, **kwargs):
        """Get context data"""

        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of last instance metrics
        context['instance_metrics'] = InstanceParameterMetric.objects.filter(
            instance=context['instance'],
            snapshot=context['instance'].snapshot,
            day=context['instance'].snapshot.day,
        )
        return context


class DatabaseOracleInstanceDetailView(LoginRequiredMixin, DetailView):
    """Instance detail view"""

    model = DatabaseOracleInstance
    template_name = "database/oracle/instance-oracle-database-detail.html"

    def get_context_data(self, **kwargs):
        """Get context data"""

        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of last instance metrics
        context['instance_metrics'] = InstanceParameterMetric.objects.filter(
            instance=context['object'],
            snapshot=context['object'].snapshot,
            day=context['object'].snapshot.day,
        )
        return context


class AsmOracleInstanceDetailView(LoginRequiredMixin, DetailView):
    """Instance detail view"""

    model = AsmOracleInstance
    template_name = "database/oracle/instance-oracle-asm-detail.html"

    def get_context_data(self, **kwargs):
        """Get context data"""

        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        # Add in a QuerySet of last instance metrics
        context['instance_metrics'] = InstanceParameterMetric.objects.filter(
            instance=context['object'],
            snapshot=context['object'].snapshot,
            day=context['object'].snapshot.day,
        )
        return context


class DatabaseListView(LoginRequiredMixin, ListView):
    """Database list view"""

    model = Database
    paginate_by = 10
    queryset = Database.objects.select_related()


class DatabaseExportView(CSVExportView):
    """Database export view"""

    model = Database
    fields = "__all__"


class DatabaseDetailView(LoginRequiredMixin, DetailView):
    """Database detail view"""

    model = Database


class OracleDatabaseDetailView(LoginRequiredMixin, DetailView):
    """Database detail view"""

    model = OracleDatabase


class ContainerOracleDatabaseCDBDetailView(LoginRequiredMixin, DetailView):
    """Database detail view"""

    model = ContainerOracleDatabase
    template_name = "database/oracle/database_detail_cdb.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        # Add last tablespace metrics
        context['tablespaces'] = OracleTablespace.objects.annotate(
            size_metric=FilteredRelation('tablespacespacemetric', condition=Q(tablespacespacemetric__snapshot_id=F('snapshot_id')))
        ).filter(database_id=context['object'].id).select_related('size_metric')

        # Add OracleUser
        context['oracle_users'] = OracleUser.objects.annotate(
            size_metric=FilteredRelation('userspacemetric', condition=Q(userspacemetric__snapshot_id=F('snapshot_id')))
        ).filter(database_id=context['object'].id).select_related('size_metric')

        return context


class ContainerOracleDatabaseLEGDetailView(LoginRequiredMixin, DetailView):
    """Database detail view"""

    model = ContainerOracleDatabase
    template_name = "database/oracle/database_detail_leg.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        # Add last tablespace metrics
        context['tablespaces'] = OracleTablespace.objects.annotate(
            size_metric=FilteredRelation('tablespacespacemetric', condition=Q(tablespacespacemetric__snapshot_id=F('snapshot_id')))
        ).filter(database_id=context['object'].id).select_related('size_metric')

        # Add OracleUser
        context['oracle_users'] = OracleUser.objects.annotate(
            size_metric=FilteredRelation('userspacemetric', condition=Q(userspacemetric__snapshot_id=F('snapshot_id')))
        ).filter(database_id=context['object'].id).select_related('size_metric')

        return context


class PluggableOracleDatabaseDetailView(LoginRequiredMixin, DetailView):
    """Database detail view"""

    model = PluggableOracleDatabase
    template_name = "database/oracle/database_detail_pdb.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        # Add last tablespace metrics
        context['tablespaces'] = OracleTablespace.objects.annotate(
            size_metric=FilteredRelation('tablespacespacemetric', condition=Q(tablespacespacemetric__snapshot_id=F('snapshot_id')))
        ).filter(database_id=context['object'].id).select_related('size_metric')

        # Add OracleUser
        context['oracle_users'] = OracleUser.objects.annotate(
            size_metric=FilteredRelation('userspacemetric', condition=Q(userspacemetric__snapshot_id=F('snapshot_id')))
        ).filter(database_id=context['object'].id).select_related('size_metric')

        return context


class EngineListView(LoginRequiredMixin, ListView):
    """Instance binary list view"""

    model = Engine
    paginate_by = 10
    queryset = Engine.objects.select_related()


class EngineDetailView(LoginRequiredMixin, DetailView):
    """Instance binary detail view"""

    model = Engine


class TablespaceListView(LoginRequiredMixin, ListView):
    """Tablespace list view"""

    model = Tablespace
    paginate_by = 10


class TablespaceExportView(CSVExportView):
    """Tablespace export view"""

    model = Tablespace
    fields = "__all__"


class TablespaceDetailView(LoginRequiredMixin, DetailView):
    """Tablespace detail view"""

    model = Tablespace


class OracleTablespaceDetailView(LoginRequiredMixin, DetailView):
    """Tablespace detail view"""

    model = OracleTablespace
    template_name = "database/oracle/tablespace_detail.html"


class UserListView(LoginRequiredMixin, ListView):
    """User list view"""

    model = User
    paginate_by = 10
    queryset = User.objects.select_related()

class UserExportView(CSVExportView):
    """User export view"""

    model = User
    fields = "__all__"



class UserDetailView(LoginRequiredMixin, DetailView):
    """User detail view"""

    model = User


class OracleUserDetailView(LoginRequiredMixin, DetailView):
    """User detail view"""

    model = OracleUser
    template_name = "database/oracle/user_detail.html"


class OracleDiskgroupDetailView(LoginRequiredMixin, DetailView):
    """Diskgroup detail view"""

    model = OracleDiskgroup
    template_name = "database/oracle/diskgroup_detail.html"


class ServiceDetailView(LoginRequiredMixin, DetailView):
    """Service detail view"""

    model = Service
    template_name = "database/service_detail.html"


class LocalListenerDetailView(LoginRequiredMixin, DetailView):
    """LocalListener detail view"""

    model = LocalListener
    template_name = "database/listener_local_detail.html"


class ScanListenerDetailView(LoginRequiredMixin, DetailView):
    """ScanOracleListener detail view"""

    model = ScanListener
    template_name = "database/listener_scan_detail.html"
