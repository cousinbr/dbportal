"""Instance base models"""

from django.db import models
from django.urls import reverse

from core.models import Environment, Metric, Snapshot, Vendor
from database.models import DatabaseType
from database.models.engine import Engine
from system.models import Server


# pylint: disable=R0903
class InstanceManager(models.Manager):
    """Instance manager"""

    def search(self, query=None):
        """Search for instance"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = models.Q(name__icontains=query)
            # distinct() is often necessary with Q lookups
            qs = qs.filter(or_lookup).distinct()
        return qs


class Instance(models.Model):
    """Base instance model"""

    name = models.CharField('Name', max_length=255)
    type = models.CharField(max_length=3, choices=DatabaseType.TYPE)

    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    engine = models.ForeignKey(Engine, on_delete=models.CASCADE)
    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE)
    environment = models.ForeignKey(Environment, on_delete=models.CASCADE, blank=True, null=True)

    objects = InstanceManager()

    class Meta:
        db_table = 'database_instance'
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(
                fields=['name', 'server'], name='instance_set_unique')
        ]

    def __str__(self):
        """Return string representation"""
        return f"{self.name} ({self.get_type_display()})"

    def get_absolute_url(self):
        """Return object absolute url"""

        if self.type == DatabaseType.ORACLE:
            return self.oracleinstance.get_absolute_url()  # pylint: disable=no-member

        return reverse('database:instance-detail', kwargs={'pk': self.pk})


class InstanceParameterMetric(Metric):
    """Instance parameter metric model"""

    instance = models.ForeignKey(Instance, on_delete=models.CASCADE)
    name = models.CharField('Name', max_length=80)
    value = models.TextField('Value')

    class Meta(Metric.Meta):
        db_table = 'database_instance_parameter_metric'
        ordering = ['name']
