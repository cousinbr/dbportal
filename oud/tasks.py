"""Oud app tasks"""

import logging
from itertools import chain

from celery import shared_task

from database.models.database import DatabaseReplicationSystem
from database.models.database.oracle import ContainerOracleDatabase
from database.models.tns import Service
from dbportal import settings
from oud.utils.oudtns import OudTns

logger = logging.getLogger(__name__)


@shared_task
def sync_oud_tns():
    """Sync OUD TNS"""

    def dict_compare(dict_1, dict_2):
        dict_1_keys = set(dict_1.keys())
        dict_2_keys = set(dict_2.keys())
        shared_keys = dict_1_keys.intersection(dict_2_keys)
        added = dict_1_keys - dict_2_keys
        removed = dict_2_keys - dict_1_keys
        modified = {o: (dict_1[o], dict_2[o]) for o in shared_keys if dict_1[o] != dict_2[o]}
        same = set(o for o in shared_keys if dict_1[o] == dict_2[o])
        return added, removed, modified, same

    #############################################################
    # Retreive data from OUD
    #############################################################
    logger.info("Retreive data from OUD")
    oud = OudTns(
        servers=settings.OUD_SERVERSLIST,
        port=settings.OUD_PORT,
        user=settings.OUD_USER,
        password=settings.OUD_PASSWORD,
        base_dn=settings.OUD_BASEDN
    )
    result = oud.getservices()

    data_oud = {}

    for dummy, value in result.items():
        data_oud[value['cn']] = value['orclNetDescString']

    #############################################################
    # Retreive data from inventory
    #############################################################
    logger.info("Retreive data from inventory")
    data_inventory = {}

    # Iter over replication systems
    replication_systems = DatabaseReplicationSystem.objects.all()

    separator = "_"
    for replication_system in replication_systems:
        logger.debug("Replication system : #%d %s",
                     replication_system.id, replication_system.name)
        # Iter over params for cluster and standalone database
        for is_cluster_database, listener_type in [(True, "SCAN"), (False, "LOCAL")]:

            # Retreive standalone databases in this replication system
            databases = replication_system.containeroracledatabase_set.filter(
                is_cluster_database=is_cluster_database)

            for database in databases:
                logger.debug("  Database : #%d %s", database.id, database)
                # Retreive database site
                database_sites = database.get_sites()

                # Retreive database host
                dummy, database_host_name = database.get_host()

                # Retreive service for container database
                services = database.service_set.all()

                # If this is a CDB, retreive services for all child PDBs
                if database.container_type == ContainerOracleDatabase.CDB:
                    pdbs = database.pluggableoracledatabase_set.all()
                    for pdb in pdbs:
                        logger.info("    PDB : #%d %s", pdb.id, pdb)
                        pdb_services = pdb.service_set.all()

                        services = list(chain(services, pdb_services))

                # Iter over services for this database
                for service in services:
                    logger.debug("    Service : #%d %s", service.id, service)
                    # Retreive one scan listener serving this service
                    listener = service.listeners.filter(
                        type=listener_type).first()
                    if listener:

                        for endpoint in listener.endpoint_set.all():
                            key = f"{database_host_name}{separator}{service.name}{separator}{database_sites[0]}"\
                                .replace('.', '_')
                            data_inventory[key] = \
                                f"(DESCRIPTION=(TRANSPORT_CONNECT_TIMEOUT=32)(CONNECT_TIMEOUT=60)(RETRY_COUNT=1)\
(ADDRESS=(PROTOCOL=TCP)(HOST={endpoint.hostname})(PORT={endpoint.port}))\
(CONNECT_DATA=(SERVICE_NAME={service.name})))"

    added, removed, modified, same = dict_compare(data_inventory, data_oud)

    logger.info("Added : %d, Removed : %d, Modified : %d, Same : %d",
                len(added), len(removed), len(modified), len(same))

    # Add new entry
    logger.info("Add new entry to OUD")
    for entry in added:
        tnsname = entry
        descstring = data_inventory[entry]
        oud.addservice(tnsname=tnsname, descstring=descstring)

    # Remove deleted entry
    logger.info("Remove deleted entry from OUD")
    for entry in removed:
        tnsname = entry
        oud.removeservice(tnsname=tnsname, removealiases=True)

    # Update modified entry
    logger.info("Update modified entry from OUD")
    for entry in modified:
        tnsname = entry
        descstring = data_inventory[entry]
        oud.modifyservice(tnsname=tnsname, descstring=descstring)

    # Alias cleanup
    logger.info("Cleanup orphan alias in OUD")
    defunct_alias = oud.getdefunctaliases()
    for entry in defunct_alias:
        oud.removealias(tnsname=entry)
