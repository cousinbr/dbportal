"""inventory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path

import django_saml2_auth.views

from dbportal import settings
from search.views import SearchView


urlpatterns = [
    # SAML path
    re_path(r'^saml2_auth/', include('django_saml2_auth.urls')),
    re_path(r'^accounts/login/$', django_saml2_auth.views.signin, name='login-saml'),

    path('', include('core.urls.browser')),
    path('database/', include('database.urls.browser')),
    path('system/', include('system.urls.browser')),
    path('zdlra/', include('zdlra.urls.browser')),
    path('search/', SearchView.as_view(), name='search'),
    path('admin/', admin.site.urls),
    path('api/', include('dbportal.urls.api')),
    path('api-auth/', include('rest_framework.urls')),
]
# Add debug toolbar urls if debug enabled
if settings.DEBUG:
    urlpatterns.append(path('__debug__/', include('debug_toolbar.urls')))
