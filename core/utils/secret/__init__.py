"""Abstraction layer for secret retreival"""

import importlib
from core.utils.secret.backend import SecretBackend

from dbportal import settings


class SecretEngine:
    """Secret engine class"""

    def __new__(cls):
        raise TypeError('Static classes cannot be instantiated')

    @staticmethod
    def get_engine() -> SecretBackend:
        """Return a secret engine"""

        secret_engine = settings.SECRET_ENGINE['backend']
        module_name, class_name = secret_engine.rsplit(".", 1)
        engine_class = getattr(
            importlib.import_module(module_name), class_name)

        assert issubclass(engine_class, SecretBackend),\
               f"{settings.SECRET_ENGINE['backend']} must derive from core.utils.password.backend.PasswordBackend"

        return engine_class()
