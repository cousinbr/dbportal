"""Secret backend ABC and dummy implementation"""

from abc import ABC, abstractmethod


class SecretBackend(ABC):
    """Secret backend ABC"""

    @abstractmethod
    def get_secret(self, key: str) -> str:
        """Return secret for a user"""


class DummySecretBackend(SecretBackend):
    """Dummy secret backen implementation"""

    def __init__(self):
        self.secrets: dict = {'key': "secret"}

    def get_secret(self, key: str, **kwargs) -> str:
        if key in self.secrets:
            return self.secrets[key]
        return ""
