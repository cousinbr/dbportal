"""Zdlra app tasks"""

from datetime import datetime
import logging

import oracledb

from celery import shared_task

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils.timezone import make_aware

from core.models import Snapshot
from database.models.database.oracle import ContainerOracleDatabase, OracleDatabase
from zdlra.models import Zdlra, ZdlraDatabase, ZdlraDatabaseSpaceMetric, ZdlraProtectionPolicy, ZdlraSpaceMetric

logger = logging.getLogger(__name__)

@shared_task
def load_zdlra_facts():
    """Load zdlra facts task"""

    zdlras = settings.ZDLRA_CONFIG

    for zdlra in zdlras:
        logger.info(f"Load ZDLRA facts : {zdlra['name']}")
        #############################################################
        # Protection policy
        #############################################################
        snapshot = Snapshot.objects.create(payload='{"'+zdlra['name']+' data load"}')

        with oracledb.connect(user=zdlra['user'], password=zdlra['password'],
                              dsn=zdlra['connect_string']) as connection:
            with connection.cursor() as cursor:

                #############################################################
                # ZDLRA
                #############################################################
                zdlra_obj, zdlra_created = Zdlra.objects.update_or_create(  # pylint: disable=unused-variable
                    name=zdlra['name'],
                    defaults={
                        'snapshot': snapshot,
                    },
                )

                logger.info(f"Load ZDLRA space metric")
                sql = """select
                            total_space,
                            used_space
                        from
                            ra_storage_location"""
                for result in cursor.execute(sql):
                    ZdlraSpaceMetric.objects.create(
                        size=result[0],
                        used=result[1],
                        zdlra=zdlra_obj,
                        snapshot=snapshot,
                        day=snapshot.day
                    )
                #############################################################
                # Protection policy
                #############################################################
                logger.info(f"Load protection policies")
                sql = """select
                            policy_name,
                            recovery_window_goal
                        from
                            ra_protection_policy"""
                protection_policies = {}
                for result in cursor.execute(sql):
                    logger.debug(f"Load protection policy : {result[0]}")
                    protection_policy_obj, protection_policy_created = ZdlraProtectionPolicy.objects.update_or_create(  # pylint: disable=unused-variable
                        name=result[0],
                        zdlra=zdlra_obj,
                        defaults={
                            'recovery_window_goal': result[1],
                        },
                    )
                    protection_policies[protection_policy_obj.name] = protection_policy_obj

                #############################################################
                # Database
                #############################################################
                logger.info(f"Load databases")
                sql = """select distinct
                            db.db_unique_name,
                            db.dbid,
                            db.state,
                            db.creation_time,
                            db.policy_name,
                            db.recovery_window_goal,
                            db.space_usage,
                            db.disk_reserved_space,
                            db.size_estimate,
                            db.recovery_window_space,
                            db.restore_window,
                            db.unprotected_window,
                            db.minimum_recovery_needed,
                            db.nzdl_active
                        from
                            ra_database_synonym s
                        join
                            ra_database db
                            on s.dbid = db.dbid"""
                for result in cursor.execute(sql):
                    logger.debug(f"Load database : {result[0]}, {result[1]}")

                    try:
                        database = OracleDatabase.objects.get(name__iexact=result[0], dbid=result[1])
                    except ObjectDoesNotExist:
                        logger.error(f"RA database not found in inventory : name={result[0]}, dbid={result[1]}")
                        continue

                    zdlra_database_obj, zdlra_database_created = ZdlraDatabase.objects.update_or_create(  # pylint: disable=unused-variable
                        database=database,
                        zdlra=zdlra_obj,
                        defaults={
                            'state': result[2],
                            'creation_time': make_aware(result[3]),
                            'nzdl_active': result[13],
                            'protection_policy': protection_policies[result[4]],
                            'snapshot': snapshot,
                        },
                    )

                    logger.debug(f"Load database : {result[0]}, {result[1]}")
                    ZdlraDatabaseSpaceMetric.objects.create(
                        space_usage=result[6],
                        disk_reserved_space=result[7],
                        size_estimate=result[8],
                        recovery_window_space=result[9],
                        restore_window=result[10],
                        unprotected_window=result[11],
                        minimum_recovery_needed=result[12],
                        zdlradatabase=zdlra_database_obj,
                        snapshot=snapshot,
                        day=snapshot.day
                    )

        snapshot.status = 'SUCCESS'
        snapshot.processed_date = make_aware(datetime.now())
        snapshot.save()


@shared_task
def load_zdlra_space_usage_history():
    """Load zdlra space usage history from ra_storage_location_history"""

    zdlras = settings.ZDLRA_CONFIG

    for zdlra in zdlras:
        with oracledb.connect(user=zdlra['user'], password=zdlra['password'],
                              dsn=zdlra['connect_string']) as connection:
            with connection.cursor() as cursor:
                zdlra_obj = Zdlra.objects.get(name=zdlra['name'])

                #############################################################
                # ZDLRA
                #############################################################

                # Create snapshot
                snapshot = Snapshot.objects.create(
                    payload='{"ZDLRA data load for '+zdlra['name']+'"}'
                )

                sql = """select
                            sample_time,
                            total_space,
                            used_space
                        from
                            ra_storage_location_history"""
                for result in cursor.execute(sql):
                    ZdlraSpaceMetric.objects.create(
                        date=make_aware(result[0]),
                        size=result[1],
                        used=result[2],
                        zdlra=zdlra_obj,
                        snapshot=snapshot,
                        day=snapshot.day
                    )

                    snapshot.status = 'SUCCESS'
                    snapshot.processed_date = make_aware(datetime.now())
                    snapshot.save()
