"""System module models Test"""
from django.db.models import Q
from django.test import TestCase

from system.models import Cluster, Devices, Filesystem, IpAddress, NetworkInterface, Server
from system.tests import ModelBuilder


class ClusterManagerTestCase(TestCase):
    """ClusterManager model test"""

    def test_search(self):
        """ClusterManager search"""
        qs = Cluster.objects.get_queryset()
        or_lookup = Q(name__icontains="CLuster")
        qs = qs.filter(or_lookup).distinct()
        self.assertQuerysetEqual(Cluster.objects.search("Cluster"), qs)

        qs = Cluster.objects.get_queryset()
        self.assertQuerysetEqual(Cluster.objects.search(), qs)


class ClusterTestCase(TestCase):
    """Cluster model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Cluster string representation"""
        obj = Cluster.objects.get(name=self.model_builder.cluster_name)
        self.assertEqual(str(obj), f"{self.model_builder.cluster_name}")

    def test_get_absolute_url(self):
        """Cluster get_absolute_url"""
        obj = Cluster.objects.get(name=self.model_builder.cluster_name)
        self.assertEqual(obj.get_absolute_url(), "/system/cluster/1/")


class ServerManagerTestCase(TestCase):
    """ServerManager model test"""

    def test_search(self):
        """ServerManager search"""
        qs = Server.objects.get_queryset()
        or_lookup = Q(name__icontains="SERVER")
        qs = qs.filter(or_lookup).distinct()
        self.assertQuerysetEqual(Server.objects.search("SERVER"), qs)

        qs = Server.objects.get_queryset()
        self.assertQuerysetEqual(Server.objects.search(), qs)


class ServerTestCase(TestCase):
    """Server model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Server string representation"""
        obj = Server.objects.get(name=self.model_builder.server_name)
        self.assertEqual(str(obj), f"{self.model_builder.server_name}")

    def test_get_absolute_url(self):
        """Server get_absolute_url"""
        obj = Server.objects.get(name=self.model_builder.server_name)
        self.assertEqual(obj.get_absolute_url(), "/system/server/1/")


class FilesystemTestCase(TestCase):
    """Filesystem model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Server string representation"""
        obj = Filesystem.objects.get(mount_point=self.model_builder.filesystem_mount_point)
        self.assertEqual(str(obj), f"{self.model_builder.filesystem_mount_point} ({self.model_builder.filesystem_type})")

    def test_get_absolute_url(self):
        """Server get_absolute_url"""
        obj = Filesystem.objects.get(mount_point=self.model_builder.filesystem_mount_point)
        self.assertEqual(obj.get_absolute_url(), "/system/filesystem/1/")


class DevicesTestCase(TestCase):
    """Devices model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Devices string representation"""
        obj = Devices.objects.get(name=self.model_builder.device_name)
        self.assertEqual(str(obj), f"{self.model_builder.device_name} ({self.model_builder.device_size_gb})")


class NetworkInterfaceTestCase(TestCase):
    """NetworkInterface model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """NetworkInterface string representation"""
        obj = NetworkInterface.objects.get(name=self.model_builder.netif_name)
        self.assertEqual(str(obj), f"{self.model_builder.netif_name}")


class IpAddressTestCase(TestCase):
    """IpAddress model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str_primary(self):
        """IpAddress string representation"""
        obj = IpAddress.objects.get(address=self.model_builder.ipaddress_address_primary)
        self.assertEqual(str(obj), f"{self.model_builder.ipaddress_address_primary}")

    def test_str_secondary(self):
        """IpAddress string representation"""
        obj = IpAddress.objects.get(address=self.model_builder.ipaddress_address_secondary)
        self.assertEqual(str(obj), f"{self.model_builder.ipaddress_address_secondary} (VIP)")
