"""Core app serializer"""
import dataclasses
from datetime import datetime

from django.utils.timezone import make_aware
from rest_framework import serializers

from core.models import Snapshot
from core.tasks import load_facts
from system.models import Server


@dataclasses.dataclass
class Task:
    """Task class"""

    id: int  # pylint: disable=invalid-name
    name: str
    payload: str
    status: str


class TaskSerializer(serializers.Serializer):
    """Task serializer"""

    id = serializers.CharField(
        max_length=256, read_only=True)  # pylint: disable=no-member
    name = serializers.CharField(max_length=256)
    payload = serializers.JSONField()
    status = serializers.CharField(max_length=16)

    def create(self, validated_data):

        # Create or get server
        server_obj, server_created = Server.objects.get_or_create(  # pylint: disable=unused-variable
            name=validated_data['name'],
        )

        # If fact gathering failed
        if validated_data['status'] == "failed":
            # Create a snapshot
            snapshot = Snapshot.objects.create(
                payload=validated_data['payload'],
                status=Snapshot.StatusChoices.FAILED,
                target_server=server_obj,
                processed_date=make_aware(datetime.now()),
                error=validated_data['payload']['module_stdout']
            )
            task = Task(id=None, **validated_data)
        else:
            # Create a snapshot
            snapshot = Snapshot.objects.create(
                payload=validated_data['payload'],
                target_server=server_obj
            )
            # Create fact loading tasks
            task_id = load_facts.delay(
                server_fqdn=validated_data['name'], snapshot_id=snapshot.pk)
            task = Task(id=task_id, **validated_data)

        return task

    def update(self, instance, validated_data):
        pass


@dataclasses.dataclass
class Inventory:
    """Inventory object"""

    inventory: dict


class AnsibleInventorySerializer(serializers.Serializer):
    """Inventory serializer for Ansible"""

    inventory = serializers.JSONField(read_only=True)

    def create(self, validated_data):
        data = {
            "_meta": {
                "hostvars": {}
            },
            "all": {
                "hosts": [],
                "children": [
                    "ungrouped"
                ]
            },
            "ungrouped": {
                "hosts": [],
                "children": []
            }
        }

        servers = Server.objects.all().select_related("environment")
        for server in servers:
            data['all']['hosts'].append(server.name)
            data['ungrouped']['hosts'].append(server.name)

            # If server have an environment
            if server.environment:

                # If env don't exist in data dict, create it
                if server.environment.code not in data:
                    data[server.environment.code] = {"hosts": [],
                                                     "children": []}

                data[server.environment.code]['hosts'].append(server.name)

        return Inventory(inventory=data)

    def update(self, instance, validated_data):
        pass
