"""Oracle Diskgroup models"""

from django.db import models
from django.urls import reverse
from core.models import Metric
from database.models.instance.oracle import AsmOracleInstance


class OracleDiskgroup(models.Model):
    """Oracle diskgroup"""

    name = models.CharField('Name', max_length=255)
    type = models.CharField('Type', max_length=6)
    compatibility = models.CharField('Compatibility', max_length=30)
    database_compatibility = models.CharField('Database compatibility', max_length=30)
    voting_files = models.CharField('Voting files', max_length=1)
    cluster_id = models.CharField('Unique cluster identifier', max_length=64)

    instances = models.ManyToManyField(AsmOracleInstance)

    class Meta:
        db_table = 'database_diskgroup_oracle'
        constraints = [
            models.UniqueConstraint(
                fields=['name', 'cluster_id'], name='diskgroup_set_unique')
        ]

    def __str__(self):
        """Return string representation"""
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:diskgroup-oracle-detail', kwargs={'pk': self.pk})

class OracleDiskgroupSizeMetric(Metric):
    """Database fra metric"""

    diskgroup = models.ForeignKey(OracleDiskgroup, on_delete=models.CASCADE)
    total_mb = models.PositiveBigIntegerField('Total size')
    used_mb = models.PositiveBigIntegerField('Used size')

    class Meta(Metric.Meta):
        db_table = 'database_diskgroup_oracle_size_metric'
