"""Core app tasks"""
from datetime import datetime
from itertools import islice
import logging
import traceback

from django.utils import timezone
from django.utils.timezone import make_aware

from celery import shared_task

from core.models import Snapshot, Vendor
from database.models import DatabaseType
from database.models.database import DatabaseReplicationSystem, DatabaseSizeMetric
from database.models.database.oracle import OracleDatabase, ContainerOracleDatabase, ContainerOracleFraMetric,\
    PluggableOracleDatabase
from database.models.diskgroup.oracle import OracleDiskgroup, OracleDiskgroupSizeMetric
from database.models.engine import Engine
from database.models.instance import InstanceParameterMetric
from database.models.instance.oracle import AsmOracleInstance, DatabaseOracleInstance
from database.models.tablespace import TablespaceSpaceMetric
from database.models.tablespace.oracle import OracleTablespace
from database.models.tns import Endpoint, Service, LocalListener, ScanListener
from database.models.user import UserSpaceMetric
from database.models.user.oracle import OracleUser
from system.models import Cluster, Devices, IpAddress, NetworkInterface, Server, Filesystem, FilesystemSizeMetric

logger = logging.getLogger(__name__)

def load_tablespace(database_obj: OracleDatabase, snapshot: Snapshot, tablespace: dict):
    """Load tablepsace"""

    #############################################################
    # Tablespace
    #############################################################
    logger.debug("Load tablespace : %s", tablespace['tablespace_name'])
    tablespace_obj, tablespace_created = OracleTablespace.objects.update_or_create(  # pylint: disable=unused-variable
        name=tablespace['tablespace_name'],
        database=database_obj,
        defaults={
            'bigfile': tablespace['bigfile'],
            'block_size': tablespace['block_size'],
            'extent_management': tablespace['extent_management'],
            'force_logging': tablespace['force_logging'],
            'segment_space_management': tablespace['segment_space_management'],
            'contents': tablespace['contents'],
            'snapshot': snapshot
        },
    )

    #############################################################
    # Tablespace metrics
    #############################################################
    if 'space_size' in tablespace and tablespace['space_size'] != '' and \
        'space_used' in tablespace and tablespace['space_used'] != '' and \
        'space_max_size' in tablespace and tablespace['space_max_size'] != '':

        logger.debug("Load tablespace metric : %s", tablespace['tablespace_name'])
        TablespaceSpaceMetric.objects.create(
            size=tablespace['space_size'],
            used=tablespace['space_used'],
            max_size=tablespace['space_max_size'],
            tablespace=tablespace_obj,
            snapshot=snapshot,
            day=snapshot.day
        )


def load_db_user(database_obj: OracleDatabase, snapshot: Snapshot, user: dict, datetime_format: str):
    """Load database users"""

    #############################################################
    # User
    #############################################################
    logger.debug("Load database user : %s", user['username'])
    defaults = {
        'account_status': user['account_status'],
        'default_tablespace': user['default_tablespace'],
        'temporary_tablespace': user['temporary_tablespace'],
        'created': make_aware(datetime.strptime(user['created'], datetime_format)),
        'profile': user['profile'],
        'password_versions': user['password_versions'],
        'authentication_type': user['authentication_type'],
        'common': user['common'],
        'snapshot': snapshot
    }
    if user['lock_date'] != "":
        defaults['lock_date'] = make_aware(datetime.strptime(user['lock_date'], datetime_format))
    if user['expiry_date'] != "":
        defaults['expiry_date'] = make_aware(datetime.strptime(user['expiry_date'], datetime_format))
    if user['last_login'] != "":
        defaults['last_login'] = make_aware(datetime.strptime(user['last_login'], datetime_format))
    if user['password_change_date'] != "":
        defaults['password_change_date'] = make_aware(datetime.strptime(user['password_change_date'], datetime_format))

    user_obj, user_created = OracleUser.objects.update_or_create(  # pylint: disable=unused-variable
        name=user['username'],
        database=database_obj,
        defaults=defaults,
    )

    #############################################################
    # User metrics
    #############################################################
    if 'schema_size' in user and user['schema_size'] != '':

        logger.debug("Load database user metric : %s", user['username'])
        UserSpaceMetric.objects.create(
            size=user['schema_size'],
            user=user_obj,
            snapshot=snapshot,
            day=snapshot.day
        )


def get_db_domain(data):
    """Retreive db_domain of one instance"""

    for parameter in data['parameters']:
        logger.debug("get_db_domain # parameter['name'] = %s", parameter['name'])
        if parameter['name'] == "db_domain":
            return parameter['value']

    # Return default
    return ""

# pylint: disable=R0912, R0914, R0915
@shared_task
def load_facts(server_fqdn: str, snapshot_id: int):
    """Core load facts"""

    try:
        logger.info("Load facts")

        # Bulk load batch size
        batch_size = 100

        # Datetime format from facts
        datetime_format = "%Y/%m/%d %H:%M:%S"

        snapshot = Snapshot.objects.get(pk=snapshot_id)

        data = snapshot.payload

        #############################################################
        # Cluster
        #############################################################
        if data['databases']['oracle']['is_cluster']:
            logger.info("Load cluster : %s", data['databases']['oracle']['cluster_name'])
            cluster_obj, cluster_created = Cluster.objects.update_or_create(  # pylint: disable=unused-variable
                name=data['databases']['oracle']['cluster_name'],
            )

        #############################################################
        # Server
        #############################################################

        # Server
        #############################################################
        logger.info("Load server : %s", server_fqdn)

        defaults = {
            'nb_cores': data['processor_cores'],
            'memtotal_mb': data['memtotal_mb'],
            'site': data['server_site'],
            'snapshot': snapshot
        }
        if data['databases']['oracle']['is_cluster']:
            defaults['cluster'] = cluster_obj

        server_obj, server_created = Server.objects.update_or_create(  # pylint: disable=unused-variable
            name=server_fqdn,
            defaults=defaults,
        )

        # Update snapshot
        #############################################################
        snapshot.target_server = server_obj
        snapshot.save()

        # Filesystem's
        #############################################################
        logger.info("Load filesystem's")
        for filesystem in data['mounts']:

            # Don't register NFS filesystem
            if filesystem['fstype'] in ('nfs'):
                continue

            logger.debug("Load filesystem : %s", filesystem['mount'])
            filesystem_obj, filesystem_created = Filesystem.objects.update_or_create(  # pylint: disable=unused-variable
                mount_point=filesystem['mount'],
                server=server_obj,
                defaults={
                    'type': filesystem['fstype'],
                    'snapshot': snapshot
                },
            )

            # Filesystem size metric
            #############################################################
            logger.debug("Load filesystem metric : %s", filesystem['mount'])
            FilesystemSizeMetric.objects.create(
                filesystem=filesystem_obj,
                size=filesystem['size_total'],
                used=filesystem['size_total']-filesystem['size_available'],
                snapshot=snapshot,
                day=snapshot.day
            )

        # Device's
        #############################################################
        logger.info("Load device's")
        for device in data['devices']:
            # Register only sd* device
            if device.startswith('sd'):
                logger.debug("Load device : %s", device)
                vendor_obj, vendor_created = Vendor.objects.update_or_create(  # pylint: disable=unused-variable
                    name=data['devices'][device]['vendor']
                )

                # Find device size
                if 'TB' in data['devices'][device]['size']:
                    size = int(float(data['devices'][device]['size'].split()[0]) * 1024)
                elif 'GB' in data['devices'][device]['size']:
                    size = int(float(data['devices'][device]['size'].split()[0]))
                else:
                    size = 0

                device_obj, device_created = Devices.objects.update_or_create(  # pylint: disable=unused-variable
                    name=device,
                    server=server_obj,
                    defaults={
                        'vendor': vendor_obj,
                        'size_gb': size
                    }
                )

        # Network interface
        #############################################################
        logger.info("Load physical network interface's")
        physical_interfaces = {}
        for interface in data['interfaces']:
            logger.debug("Current network interface : %s", interface)
            if 'type' in data[interface] and data[interface]['type'] == "ether":
                logger.debug("Load physical network interface : %s", interface)

                # Parse data and fill defaults
                defaults = {
                    'type': NetworkInterface.PHYSICAL,
                    'active': data[interface]['active']
                }
                if 'speed' in data[interface] and not data[interface]['speed'] == "":
                    defaults['speed'] = int(data[interface]['speed'])
                if 'mtu' in data[interface] and not data[interface]['mtu'] == "":
                    defaults['mtu'] = int(data[interface]['mtu'])

                netif_obj, netif_created = NetworkInterface.objects.update_or_create(  # pylint: disable=unused-variable
                    name=interface,
                    server=server_obj,
                    defaults=defaults,
                )
                # Store object for reuse
                physical_interfaces[interface] = netif_obj

                # Add IP address
                if 'ipv4' in data[interface]:
                    logger.debug("Load primary IP : %s", data[interface]['ipv4']['address'])
                    IpAddress.objects.update_or_create(
                        address=data[interface]['ipv4']['address'],
                        interface=netif_obj,
                        defaults={
                            'netmask': data[interface]['ipv4']['netmask'],
                            'network': data[interface]['ipv4']['network'],
                            'prefix': data[interface]['ipv4']['prefix'],
                            'is_primary': True,
                        }
                    )

                # Search for secondary IP on this interface
                secondary_ips = filter(lambda ip: ip.startswith(f"{interface}_"), data['interfaces'])
                for secondary_ip in secondary_ips:
                    logger.debug("Load secondary IP : %s", secondary_ip)
                    IpAddress.objects.update_or_create(
                        address=data[secondary_ip]['ipv4']['address'],
                        interface=netif_obj,
                        defaults={
                            'netmask': data[secondary_ip]['ipv4']['netmask'],
                            'network': data[secondary_ip]['ipv4']['network'],
                            'prefix': data[secondary_ip]['ipv4']['prefix'],
                            'is_primary': False,
                        }
                    )

        logger.info("Load bonded network interface's")
        for interface in data['interfaces']:
            logger.debug("Current network interface : %s", interface)
            if 'type' in data[interface] and data[interface]['type'] == "bonding":
                logger.debug("Load bonded network interface : %s", interface)

                # Parse data and fill defaults
                defaults = {
                    'type': NetworkInterface.BONDED,
                    'active': data[interface]['active']
                }
                if 'speed' in data[interface] and not data[interface]['speed'] == "":
                    defaults['speed'] = int(data[interface]['speed'])
                if 'mtu' in data[interface] and not data[interface]['mtu'] == "":
                    defaults['mtu'] = int(data[interface]['mtu'])
                if 'lacp_rate' in data[interface] and not data[interface]['lacp_rate'] == "":
                    defaults['lacp_rate'] = data[interface]['lacp_rate']
                if 'mode' in data[interface] and not data[interface]['mode'] == "":
                    defaults['mode'] = data[interface]['mode']

                netif_obj, netif_created = NetworkInterface.objects.update_or_create(  # pylint: disable=unused-variable
                    name=interface,
                    server=server_obj,
                    defaults=defaults,
                )

                for slave in data[interface]['slaves']:
                    netif_obj.slave_interfaces.add(physical_interfaces[slave])

                # Add IP address
                if 'ipv4' in data[interface]:
                    logger.debug("Load primary IP : %s", data[interface]['ipv4']['address'])
                    IpAddress.objects.update_or_create(
                        address=data[interface]['ipv4']['address'],
                        interface=netif_obj,
                        defaults={
                            'netmask': data[interface]['ipv4']['netmask'],
                            'network': data[interface]['ipv4']['network'],
                            'prefix': data[interface]['ipv4']['prefix'],
                            'is_primary': True,
                        }
                    )

                # Search for secondary IP on this interface
                secondary_ips = filter(lambda ip: ip.startswith(f"{interface}_"), data['interfaces'])
                for secondary_ip in secondary_ips:
                    logger.debug("Load secondary IP : %s", secondary_ip)
                    for secondary_ip_conf in data[secondary_ip]['ipv4_secondaries']:
                        IpAddress.objects.update_or_create(
                            address=secondary_ip_conf['address'],
                            interface=netif_obj,
                            defaults={
                                'netmask': secondary_ip_conf['netmask'],
                                'network': secondary_ip_conf['network'],
                                'prefix': secondary_ip_conf['prefix'],
                                'is_primary': False,
                            }
                        )

        #############################################################
        # Vendor
        #############################################################
        logger.info("Load vendor : Oracle")
        vendor_obj, vendor_created = Vendor.objects.update_or_create(  # pylint: disable=unused-variable
            name="Oracle"
        )

        #############################################################
        # ASM
        #############################################################
        instance = data['databases']['oracle']['instances']['asm']

        if instance:
            # ASM Engine
            #############################################################
            logger.info("Load engine : %s", instance['oracle_home'])
            engine_obj, instance_binary_created = Engine.objects.update_or_create(  # pylint: disable=unused-variable
                path=instance['oracle_home'],
                server=server_obj,
                defaults={
                    'version': instance['version']
                },
            )

            # ASM Instance
            #############################################################
            logger.info("Load ASM instance : %s", instance['sid'])
            instance_obj, instance_created = AsmOracleInstance.objects.update_or_create(  # pylint: disable=unused-variable
                name=instance['sid'],
                server=server_obj,
                defaults={
                    'engine': engine_obj,
                    'vendor': vendor_obj,
                    'snapshot': snapshot
                },
            )

            # ASM Instance parameter
            #############################################################
            logger.info("Load ASM instance parameter")
            objs = (InstanceParameterMetric(
                name=parameter['name'],
                value=parameter['value'],
                instance=instance_obj,
                snapshot=snapshot,
                day=snapshot.day)
                for parameter in instance['parameters'])
            while True:
                batch = list(islice(objs, batch_size))
                if not batch:
                    break
                InstanceParameterMetric.objects.bulk_create(batch, batch_size)

            # ASM diskgroup
            #############################################################
            logger.info("Load ASM diskgroup's")
            for diskgroup in instance['diskgroups']:

                # ASM diskgroup
                #############################################################
                logger.debug("Load ASM diskgroup : %s", diskgroup['name'])
                diskgroup_obj, diskgroup_created = OracleDiskgroup.objects.update_or_create(  # pylint: disable=unused-variable
                    name=diskgroup['name'],
                    cluster_id=diskgroup['cluster_id'],
                    defaults={
                        'type': diskgroup['type'],
                        'compatibility': diskgroup['compatibility'],
                        'database_compatibility': diskgroup['database_compatibility'],
                        'voting_files': diskgroup['voting_files']
                    },
                )
                diskgroup_obj.instances.add(instance_obj)

                # ASM diskgroup size metric
                #############################################################
                logger.debug("Load ASM diskgroup metric : %s", diskgroup['name'])
                OracleDiskgroupSizeMetric.objects.update_or_create(
                    date=timezone.now().replace(minute=0, second=0, microsecond=0),
                    diskgroup=diskgroup_obj,
                    defaults={
                        'total_mb': diskgroup['total_mb'],
                        'used_mb': int(diskgroup['total_mb'])-int(diskgroup['free_mb']),
                        'snapshot': snapshot,
                        'day': snapshot.day
                    }
                )

        #############################################################
        # RDBMS instance
        #############################################################
        for instance in data['databases']['oracle']['instances']['rdbms']:

            #############################################################
            # Engine
            #############################################################
            logger.info("Load engine : %s", instance['oracle_home'])
            engine_obj, instance_binary_created = Engine.objects.update_or_create(  # pylint: disable=unused-variable
                path=instance['oracle_home'],
                server=server_obj,
                defaults={
                    'version': instance['version']
                },
            )

            #############################################################
            # Instance
            #############################################################
            logger.info("Load instance : %s", instance['sid'])
            instance_obj, instance_created = DatabaseOracleInstance.objects.update_or_create(  # pylint: disable=unused-variable
                name=instance['sid'],
                server=server_obj,
                defaults={
                    'engine': engine_obj,
                    'vendor': vendor_obj,
                    'snapshot': snapshot
                },
            )

            #############################################################
            # Instance parameter
            #############################################################
            logger.info("Load instance parameter")
            objs = (InstanceParameterMetric(
                name=parameter['name'],
                value=parameter['value'],
                instance=instance_obj,
                snapshot=snapshot,
                day=snapshot.day)
                for parameter in instance['parameters'])
            while True:
                batch = list(islice(objs, batch_size))
                if not batch:
                    break
                InstanceParameterMetric.objects.bulk_create(batch, batch_size)

            #############################################################
            # Database
            #############################################################
            if instance['database']:

                logger.info("Load replication system")
                # Create
                replication_system_obj, replication_system_created = DatabaseReplicationSystem.objects.get_or_create(  # pylint: disable=unused-variable
                    name=instance['database']['db_name'],
                    system_id=instance['database']['dataguard_database_id'],
                )

                # Identify database type
                container_type = ContainerOracleDatabase.CDB
                if not instance['database']['is_cdb']:
                    container_type = ContainerOracleDatabase.LEGACY

                logger.info("Load container database : %s", instance['database']['db_unique_name'])
                database_obj, database_created = ContainerOracleDatabase.objects.update_or_create(  # pylint: disable=unused-variable
                    name=instance['database']['db_unique_name'],
                    type=DatabaseType.ORACLE,
                    cluster_id=instance['database']['cluster_id'],
                    defaults={
                        'global_name': instance['database']['db_name'],
                        'dbid': instance['database']['dbid'],
                        'is_cluster_database': instance['database']['is_cluster_database'],
                        'created': make_aware(datetime.strptime(instance['database']['created'], datetime_format)),
                        'resetlogs_time': make_aware(
                            datetime.strptime(instance['database']['resetlogs_time'], datetime_format)),
                        'log_mode': instance['database']['log_mode'],
                        'database_role': instance['database']['database_role'],
                        'force_logging': instance['database']['force_logging'],
                        'flashback_on': instance['database']['flashback_on'],
                        'replication_system': replication_system_obj,
                        'size': instance['database']['database_size'],
                        'snapshot': snapshot,
                        'container_type': container_type
                    },
                )
                database_obj.instances.add(instance_obj)

                #############################################################
                # Database size metric
                #############################################################
                if instance['database']['database_size'] != "":

                    logger.info("Load container database size metric : %s", instance['database']['db_unique_name'])
                    DatabaseSizeMetric.objects.update_or_create(
                        date=timezone.now().replace(minute=0, second=0, microsecond=0),
                        database=database_obj,
                        defaults={
                            'size': instance['database']['database_size'],
                            'datafile_size': instance['database']['datafile_size'],
                            'redo_size': instance['database']['redo_size'],
                            'tempfile_size': instance['database']['tempfile_size'],
                            'snapshot': snapshot,
                            'day': snapshot.day
                        }
                    )

                #############################################################
                # Database FRA metric
                #############################################################
                if instance['database']['fra']['size'] != "" and instance['database']['fra']['used'] != "" and \
                        instance['database']['fra']['reclaimable'] != "":

                    logger.info("Load container database FRA metric : %s", instance['database']['db_unique_name'])
                    ContainerOracleFraMetric.objects.update_or_create(
                        date=timezone.now().replace(minute=0, second=0, microsecond=0),
                        database=database_obj,
                        defaults={
                            'size': instance['database']['fra']['size'],
                            'used': instance['database']['fra']['used'],
                            'reclaimable': instance['database']['fra']['reclaimable'],
                            'snapshot': snapshot,
                            'day': snapshot.day
                        }
                    )

                #############################################################
                # Tablespace
                #############################################################
                logger.info("Load container database tablespaces : %s", instance['database']['db_unique_name'])
                for tablespace in instance['database']['tablespaces']:
                    load_tablespace(database_obj, snapshot, tablespace)

                #############################################################
                # Users
                #############################################################
                logger.info("Load container database users : %s", instance['database']['db_unique_name'])
                for user in instance['database']['users']:
                    load_db_user(database_obj, snapshot, user, datetime_format)

                #############################################################
                # PDB's
                #############################################################
                logger.info("Load PDB's")
                for pdb in instance['pdbs']:

                    logger.debug("Load PDB : %s", pdb['name'])
                    pdb_obj, pdb_created = PluggableOracleDatabase.objects.update_or_create(  # pylint: disable=unused-variable
                        name=pdb['name'],
                        type=DatabaseType.ORACLE,
                        cluster_id=pdb['cluster_id'],
                        defaults={
                            'dbid': pdb['dbid'],
                            'created': make_aware(datetime.strptime(pdb['creation_time'], datetime_format)),
                            'size': pdb['database_size'],
                            'guid': pdb['guid'],
                            'container': database_obj,
                            'snapshot': snapshot
                        },
                    )
                    pdb_obj.instances.add(instance_obj)

                    #############################################################
                    # Database size metric
                    #############################################################
                    if pdb['database_size'] != "":

                        logger.debug("Load PDB database size metric : %s", pdb['name'])
                        DatabaseSizeMetric.objects.update_or_create(
                            date=timezone.now().replace(minute=0, second=0, microsecond=0),
                            database=pdb_obj,
                            defaults={
                                'size': pdb['database_size'],
                                'datafile_size': pdb['datafile_size'],
                                'redo_size': pdb['redo_size'],
                                'tempfile_size': pdb['tempfile_size'],
                                'snapshot': snapshot,
                                'day': snapshot.day
                            }
                        )

                    #############################################################
                    # Tablespace
                    #############################################################
                    logger.debug("Load PDB tablespaces : %s", pdb['name'])
                    for tablespace in pdb['tablespaces']:
                        load_tablespace(pdb_obj, snapshot, tablespace)

                    #############################################################
                    # Users
                    #############################################################
                    logger.debug("Load PDB users : %s", pdb['name'])
                    for user in pdb['users']:
                        load_db_user(pdb_obj, snapshot, user, datetime_format)

        #############################################################
        # Listeners
        #############################################################
        logger.info("Load listeners")
        for listener in data['databases']['oracle']['listeners']:
            logger.debug("Listener infos : %s", listener['name'])

            # SCAN Listener
            #############################################################
            if listener['type'] == "scan":
                logger.debug("Load SCAN listener")
                listener_obj, listener_created = ScanListener.objects.update_or_create(  # pylint: disable=unused-variable
                    name = listener['name'],
                    engine = Engine.objects.get(
                        path = listener['oracle_home'],
                        server = server_obj
                    ),
                    cluster = cluster_obj
                )

            # LOCAL Listener
            #############################################################
            elif listener['type'] == "local":
                logger.debug("Load LOCAL listener")
                listener_obj, listener_created = LocalListener.objects.update_or_create(  # pylint: disable=unused-variable
                    name = listener['name'],
                    engine = Engine.objects.get(
                        path = listener['oracle_home'],
                        server = server_obj
                    ),
                    server = server_obj
                )

            # Endpoints
            #############################################################
            for endpoint in listener['endpoints']:
                logger.debug("Load endpoint (hostname : %s, port : %s)", endpoint['hostname'], endpoint['port'])
                endpoint_obj, endpoint_created = Endpoint.objects.update_or_create(  # pylint: disable=unused-variable
                    hostname = endpoint['hostname'],
                    port = endpoint['port'],
                    listener = listener_obj
                )

            # Services
            #############################################################
            for tns_service in listener['services']:
                logger.debug("Load TNS service \"%s\"", tns_service['name'])
                found_database = None

                # Try to match a service found on listener to a database instance
                #############################################################
                for instance in data['databases']['oracle']['instances']['rdbms']:

                    # If inventory instance match service instance
                    if instance['sid'] in tns_service['instances']:

                        logger.debug("Found matching instance : %s", instance['sid'])

                        # Retreive db_domain
                        db_domain = get_db_domain(data=instance)
                        logger.debug("Founded db_domain : \"%s\"", db_domain)
                        if db_domain != "":
                            db_domain = f".{db_domain}"

                        found_database = None

                        # Iterate over this database services
                        for db_service in instance['database']['services']:

                            # Skip service with network name empty
                            if db_service['network_name'] == "":
                                logger.debug(f"Skip service {db_service['name']}, network_name empty")
                                continue

                            # Remove db_domain from network_name if present
                            service_to_test = db_service['network_name']
                            if service_to_test.endswith(db_domain):
                                service_to_test = service_to_test.replace(db_domain, '')

                            logger.debug(
                                "Compare : %s, %s", tns_service['name'],
                                f"{service_to_test}{db_domain}"
                            )
                            if tns_service['name'] == f"{service_to_test}{db_domain}":
                                found_database = {
                                    'type': 'CDB',
                                    'name': instance['database']['db_unique_name']
                                }
                                break

                        # Iterate over this database PDB's if database not found
                        if not found_database:
                            for pdb in instance['pdbs']:
                                logger.debug("Search service in PDB : %s", pdb['name'])
                                # Iterate over this PDB services
                                for db_service in pdb['services']:

                                    # Remove db_domain from network_name if present
                                    service_to_test = db_service['network_name']
                                    if service_to_test.endswith(db_domain):
                                        service_to_test = service_to_test.replace(db_domain, '')

                                    logger.debug(
                                        "Compare : %s, %s", tns_service['name'],
                                        f"{service_to_test}{db_domain}"
                                    )
                                    if tns_service['name'] == f"{service_to_test}{db_domain}":
                                        found_database = {
                                            'type': 'PDB',
                                            'name': pdb['name'],
                                            'cdb': instance['database']['db_unique_name']
                                        }
                                        break

                    # Database found, exist from instance loop
                    if found_database:
                        break

                # If we found a database
                if found_database:
                    # Retreive founded database object
                    if found_database['type'] == "CDB":
                        database = ContainerOracleDatabase.objects.get(
                            name=found_database['name'],
                            instances__server=server_obj
                        )
                    else:
                        database = PluggableOracleDatabase.objects.get(
                            name=found_database['name'],
                            container__name=found_database['cdb'],
                            instances__server=server_obj
                        )

                    service_obj, service_created = Service.objects.update_or_create(  # pylint: disable=unused-variable
                        name = tns_service['name'],
                        database = database
                    )
                    service_obj.listeners.add(listener_obj)

        logger.info("Set snapshot load as SUCCESS")
        snapshot.status = Snapshot.StatusChoices.SUCCESS
        snapshot.processed_date = make_aware(datetime.now())
        snapshot.save()

    except Exception as exception:
        logger.error("Snapshot load on error")
        snapshot.status = Snapshot.StatusChoices.FAILED
        snapshot.processed_date = make_aware(datetime.now())
        snapshot.error = traceback.format_exc()
        snapshot.save()

        raise exception
