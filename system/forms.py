"""System forms"""

from django.forms import ModelForm

from system.models import Server

class ServerForm(ModelForm):
    """Server form"""

    class Meta:
        model = Server
        fields = ('__all__')
        exclude = ['snapshot']
