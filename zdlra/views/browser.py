"""Zdlra browser views"""

from csv_export.views import CSVExportView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView

from zdlra.models import Zdlra, ZdlraDatabase, ZdlraProtectionPolicy, ZdlraDatabaseSpaceMetric


class ZdlraListView(LoginRequiredMixin, ListView):
    """Zdlra list view"""

    model = Zdlra


class ZdlraExportView(CSVExportView):
    """Zdlra export view"""

    model = Zdlra
    fields = "__all__"


class ZdlraDetailView(LoginRequiredMixin, DetailView):
    """Zdlra detail view"""

    model = Zdlra


class ZdlraProtectionPolicyListView(LoginRequiredMixin, ListView):
    """Zdlra protection policy list view"""

    model = ZdlraProtectionPolicy
    queryset = ZdlraProtectionPolicy.objects.all().select_related()


class ZdlraProtectionPolicyExportView(CSVExportView):
    """Zdlra protection policy export view"""

    model = ZdlraProtectionPolicy
    fields = "__all__"


class ZdlraProtectionPolicyDetailView(LoginRequiredMixin, DetailView):
    """Zdlra protection policy detail view"""

    model = ZdlraProtectionPolicy


class ZdlraDatabaseListView(LoginRequiredMixin, ListView):
    """Zdlra database list view"""

    model = ZdlraDatabase


class ZdlraDatabaseExportView(CSVExportView):
    """Database export view"""

    model = ZdlraDatabase
    fields = "__all__"


class ZdlraDatabaseDetailView(LoginRequiredMixin, DetailView):
    """Zdlra database detail view"""

    model = ZdlraDatabase

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        # Add in a QuerySet of last zdlra database metrics
        context['last_metric'] = ZdlraDatabaseSpaceMetric.objects.filter(
            zdlradatabase=context['zdlradatabase'],
            snapshot=context['zdlradatabase'].snapshot
        ).first()
        return context
