from django.db import models
from django.urls import reverse

from system.models import Server

class Engine(models.Model):
    """Instance binary model"""

    path = models.CharField('Instance\'s binary', max_length=255)
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    version = models.CharField('Version', max_length=255)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['path', 'server'], name='engine_set_unique')
        ]


    def __str__(self):
        return f"{self.path}"


    def get_absolute_url(self):
        """Return object absolute url"""

        return reverse('database:instancebinary-detail', kwargs={'pk': self.pk})
