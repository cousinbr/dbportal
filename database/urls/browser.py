"""Database brownser urls"""

from django.urls import path

from database.views.browser import AsmOracleInstanceDetailView, ContainerOracleDatabaseCDBDetailView, \
    ContainerOracleDatabaseLEGDetailView, DatabaseDetailView, DatabaseExportView, DatabaseListView, DatabaseOracleInstanceDetailView, \
    EngineDetailView, EngineListView, InstanceDetailView, InstanceExportView, InstanceListView, LocalListenerDetailView, \
    OracleDiskgroupDetailView, OracleTablespaceDetailView, OracleUserDetailView, PluggableOracleDatabaseDetailView, \
    ScanListenerDetailView, ServiceDetailView, TablespaceDetailView, TablespaceExportView, TablespaceListView, UserDetailView, UserExportView, \
    UserListView

app_name = 'database'
urlpatterns = [
    # Instance
    path('instances/', InstanceListView.as_view(), name='instance-list'),
    path('instances/export/', InstanceExportView.as_view(), name='instance-export'),
    path('instance/<int:pk>/', InstanceDetailView.as_view(), name='instance-detail'),
    path('instance/oracle/rdbms/<int:pk>/', DatabaseOracleInstanceDetailView.as_view(), name='instance-oracle-database-detail'),
    path('instance/oracle/asm/<int:pk>/', AsmOracleInstanceDetailView.as_view(), name='instance-oracle-asm-detail'),
    # Database
    path('databases/', DatabaseListView.as_view(), name='database-list'),
    path('databases/export/', DatabaseExportView.as_view(), name='database-export'),
    path('database/<int:pk>/', DatabaseDetailView.as_view(), name='database-detail'),
    path('database/oracle/legacy/<int:pk>/', ContainerOracleDatabaseLEGDetailView.as_view(), name='database-oracle-leg-detail'),
    path('database/oracle/cdb/<int:pk>/', ContainerOracleDatabaseCDBDetailView.as_view(), name='database-oracle-cdb-detail'),
    path('database/oracle/pdb/<int:pk>/', PluggableOracleDatabaseDetailView.as_view(), name='database-oracle-pdb-detail'),
    # Tablespace
    path('tablespaces/', TablespaceListView.as_view(), name='tablespace-list'),
    path('tablespaces/export/', TablespaceExportView.as_view(), name='tablespace-export'),
    path('tablespace/<int:pk>/', TablespaceDetailView.as_view(), name='tablespace-detail'),
    path('tablespace/oracle/<int:pk>/', OracleTablespaceDetailView.as_view(), name='tablespace-oracle-detail'),
    # Users
    path('users/', UserListView.as_view(), name='user-list'),
    path('users/export/', UserExportView.as_view(), name='user-export'),
    path('user/<int:pk>/', UserDetailView.as_view(), name='user-detail'),
    path('user/oracle/<int:pk>/', OracleUserDetailView.as_view(), name='user-oracle-detail'),
    # Instance Binary
    path('instance_binarys/', EngineListView.as_view(), name='instancebinary-list'),
    path('instance_binary/<int:pk>/', EngineDetailView.as_view(), name='instancebinary-detail'),
    # Diskgroup
    path('diskgroup/oracle/<int:pk>/', OracleDiskgroupDetailView.as_view(), name='diskgroup-oracle-detail'),
    # Service
    path('service/<int:pk>/', ServiceDetailView.as_view(), name='service-detail'),
    # Listener
    path('listener/oracle/local/<int:pk>/', LocalListenerDetailView.as_view(), name='listener-local-detail'),
    path('listener/oracle/scan/<int:pk>/', ScanListenerDetailView.as_view(), name='listener-scan-detail'),
]
