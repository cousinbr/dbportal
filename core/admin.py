"""Core app Admin"""
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from rest_framework.authtoken.admin import TokenAdmin
from core.models import Environment, Snapshot, User, Vendor

TokenAdmin.raw_id_fields = ['user']

admin.site.register(User, UserAdmin)
admin.site.register(Environment)
admin.site.register(Snapshot)
admin.site.register(Vendor)
