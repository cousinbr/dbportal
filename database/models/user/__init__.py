from django.db import models
from django.urls import reverse

from core.models import Metric, Snapshot
from database.models import DatabaseType
from database.models.database import Database


# pylint: disable=R0903
class UserManager(models.Manager):
    """User manager"""

    def search(self, query=None):
        """Search for user"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = (
                models.Q(name__icontains=query)
            )
            # distinct() is often necessary with Q lookups
            qs = qs.filter(or_lookup).distinct()
        return qs


class User(models.Model):
    """User model"""

    name = models.CharField('Username', max_length=128)
    type = models.CharField(max_length=3, choices=DatabaseType.TYPE)

    database = models.ForeignKey(Database, on_delete=models.CASCADE)
    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE)

    objects = UserManager()

    class Meta:
        db_table = 'database_user'
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(
                fields=['database', 'name'], name='user_set_unique')
        ]

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""

        if self.type == DatabaseType.ORACLE:
            return self.oracleuser.get_absolute_url()


class UserSpaceMetric(Metric):
    """Tablespace space metric"""

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    size = models.PositiveBigIntegerField('Size')

    class Meta(Metric.Meta):
        db_table = 'database_user_space_metric'
