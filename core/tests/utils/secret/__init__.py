"""Core app utils.secret Test"""
from django.test import TestCase

from core.utils.secret import SecretEngine
from core.utils.secret.backend import SecretBackend


class SecretEngineTestCase(TestCase):
    """SecretEngine test"""

    def test_is_static(self):
        """Test that SecretEngine could not be instancied"""
        with self.assertRaises(TypeError):
            SecretEngine()

    def test_get_engine(self):
        """Test get engine"""
        engine = SecretEngine.get_engine()

        self.assertIsInstance(engine, SecretBackend)
