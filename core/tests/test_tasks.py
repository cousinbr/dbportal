"""Core app tasks Test"""
import json

from django.test import TestCase

from core.tasks import load_facts
from core.models import Snapshot


class LoadFactsTestCase(TestCase):
    """Load facts function test"""

    def setUp(self):
        # Opening JSON file
        with open('facts.json', encoding="utf8") as file:
            self.data = json.load(file)

    def test_load(self):
        """Load facts"""
        snapshot = Snapshot.objects.create(payload=self.data)
        load_facts('server.domain', snapshot_id=snapshot.id)
        self.assertEqual(1, 1)
