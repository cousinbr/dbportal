"""Database module models Test"""

from django.db.models import Q
from django.test import TestCase

from database.models.instance import Instance
from database.models.instance.oracle import AsmOracleInstance, DatabaseOracleInstance
from database.tests.models import ModelBuilder


class InstanceManagerTestCase(TestCase):
    """InstanceManager model test"""

    def test_search(self):
        """InstanceManager search"""
        qs = Instance.objects.get_queryset()
        or_lookup = Q(name__icontains="ORCL")
        qs = qs.filter(or_lookup).distinct()
        self.assertQuerysetEqual(Instance.objects.search("ORCL"), qs)

        qs = Instance.objects.get_queryset()
        self.assertQuerysetEqual(Instance.objects.search(), qs)


class AsmOracleInstanceTestCase(TestCase):
    """Instance model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Instance string representation"""
        obj = AsmOracleInstance.objects.get(name=self.model_builder.asm_oracle_instance_name)
        self.assertEqual(str(obj), f"{self.model_builder.asm_oracle_instance_name} on {self.model_builder.system_model_builder.server_obj}")

    def test_get_absolute_url(self):
        """Instance get_absolute_url"""
        obj = Instance.objects.get(name=self.model_builder.asm_oracle_instance_name)
        self.assertEqual(obj.get_absolute_url(), f"/database/instance/oracle/asm/{obj.id}/")


class DatabaseOracleInstanceTestCase(TestCase):
    """Instance model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Instance string representation"""
        obj = DatabaseOracleInstance.objects.get(name=self.model_builder.database_oracle_instance_name)
        self.assertEqual(str(obj), f"{self.model_builder.database_oracle_instance_name} on {self.model_builder.system_model_builder.server_obj}")

    def test_get_absolute_url(self):
        """Instance get_absolute_url"""
        obj = Instance.objects.get(name=self.model_builder.database_oracle_instance_name)
        self.assertEqual(obj.get_absolute_url(), f"/database/instance/oracle/rdbms/{obj.id}/")
