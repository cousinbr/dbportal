"""Zdlra app models"""
from django.db import models
from django.urls import reverse

from core.models import Metric, Snapshot
from database.models.database.oracle import OracleDatabase


class Zdlra(models.Model):
    """Zdlra model"""

    name = models.CharField('Name', max_length=128)
    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE)

    class Meta:
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(
                fields=['name'], name='zdlra_set_unique')
        ]

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""

        return reverse('zdlra:zdlra-detail', kwargs={'pk': self.pk})


class ZdlraSpaceMetric(Metric):
    """Zdlra space metric"""

    zdlra = models.ForeignKey(Zdlra, on_delete=models.CASCADE)
    size = models.PositiveIntegerField('Size')
    used = models.PositiveIntegerField('Used')

    class Meta(Metric.Meta):
        db_table = 'zdlra_zdlra_space_metric'

class ZdlraProtectionPolicy(models.Model):
    """Zdlra protection policy model"""

    name = models.CharField('Name', max_length=128)
    recovery_window_goal = models.DurationField('Recovery window goal')

    zdlra = models.ForeignKey(Zdlra, on_delete=models.CASCADE)

    class Meta:
        db_table = 'zdlra_zdlra_protection_policy'
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(
                fields=['name', 'zdlra'], name='zdlraprotectionpolicy_set_unique')
        ]

    def __str__(self):
        return f"{self.name} ({self.recovery_window_goal}) on {self.zdlra}"

    def get_absolute_url(self):
        """Return object absolute url"""

        return reverse('zdlra:zdlraprotectionpolicy-detail', kwargs={'pk': self.pk})


class ZdlraDatabase(models.Model):
    """Zdlra database model"""

    state = models.CharField('State', max_length=12)
    creation_time = models.DateTimeField('Creation time')
    nzdl_active = models.CharField('Realtime redolog transport active', max_length=3)

    zdlra = models.ForeignKey(Zdlra, on_delete=models.CASCADE)
    protection_policy = models.ForeignKey(ZdlraProtectionPolicy, on_delete=models.CASCADE)
    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE)
    database = models.ForeignKey(OracleDatabase, on_delete=models.CASCADE)

    class Meta:
        db_table = 'zdlra_zdlra_database'
        ordering = ['database']
        constraints = [
            models.UniqueConstraint(
                fields=['database', 'zdlra'], name='zdlradatabase_set_unique')
        ]

    def __str__(self):
        return f"{self.database}"

    def get_absolute_url(self):
        """Return object absolute url"""

        return reverse('zdlra:zdlradatabase-detail', kwargs={'pk': self.pk})


class ZdlraDatabaseSpaceMetric(Metric):
    """Zdlra database space metric"""

    zdlradatabase = models.ForeignKey(ZdlraDatabase, on_delete=models.CASCADE)
    space_usage = models.PositiveIntegerField('Space usage')
    disk_reserved_space = models.PositiveIntegerField('Disk reserved space')
    size_estimate = models.PositiveIntegerField('Size estimate')
    recovery_window_space = models.PositiveIntegerField('Recovery window space')
    restore_window = models.DurationField('Restore window')
    unprotected_window = models.DurationField('Unprotected window')
    minimum_recovery_needed = models.DurationField('Minimum recovery needed')

    class Meta(Metric.Meta):
        db_table = 'zdlra_zdlra_database_space_metric'
