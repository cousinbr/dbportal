from django.db import models

from core.models import Metric
from database.models.instance import Instance

class DiskgroupParameterMetric(Metric):
    """Diskgroup parameter metric model"""

    instance = models.ForeignKey(Instance, on_delete=models.CASCADE)
    name = models.CharField('Name', max_length=80)
    value = models.TextField('Value')

    class Meta(Metric.Meta):
        db_table = 'database_diskgroup_parameter_metric'
        ordering = ['name']
