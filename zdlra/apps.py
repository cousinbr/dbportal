"""Zdlra module apps"""
from django.apps import AppConfig


class ZdlraConfig(AppConfig):
    """Zdlra app config"""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'zdlra'
