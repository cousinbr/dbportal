"""Database module Admin"""
from django.contrib import admin

from database.models.database import Database
from database.models.instance import Instance

admin.site.register(Database)
admin.site.register(Instance)
