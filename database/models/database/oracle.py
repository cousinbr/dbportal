"""Database Oracle models"""

from django.db import models
from django.urls import reverse

from core.models import Metric
from database.models import DatabaseType
from database.models.database import DatabaseReplicationSystem
from database.models.database import Database


class OracleDatabase(Database):
    """Base Oracle database model"""

    CDB = 'CDB'
    PDB = 'PDB'

    ORACLE_DATABASE_TYPE = [
        (CDB, 'Container Database'),
        (PDB, 'Pluggable Database'),
    ]

    database_type = models.CharField(max_length=3, choices=ORACLE_DATABASE_TYPE)
    dbid = models.PositiveBigIntegerField('DBID')
    size = models.PositiveBigIntegerField('Database Size', default=0)
    created = models.DateTimeField('Creation date')

    def save(self, *args, **kwargs):
        self.type = DatabaseType.ORACLE
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        """Return object absolute url"""
        if self.database_type == self.CDB:
            return self.containeroracledatabase.get_absolute_url()  # pylint: disable=no-member
        else:
            return self.pluggableoracledatabase.get_absolute_url()  # pylint: disable=no-member

    class Meta:
        db_table = 'database_database_oracle'


class ContainerOracleDatabase(OracleDatabase):
    """Container Oracle database"""

    LEGACY = 'LEG'
    CDB = 'CDB'

    ORACLE_CONTAINER_TYPE = [
        (LEGACY, 'Legacy Database'),
        (CDB, 'Multitenant Container Database'),
    ]

    container_type = models.CharField(max_length=3, choices=ORACLE_CONTAINER_TYPE)
    global_name = models.CharField('Name', max_length=128)
    resetlogs_time = models.DateTimeField('Resetlog date')
    log_mode = models.CharField('Log mode', max_length=12)
    database_role = models.CharField('Database role', max_length=16)
    force_logging = models.CharField('Force logging', max_length=39)
    flashback_on = models.CharField('Flashback on', max_length=18)
    is_cluster_database = models.BooleanField('Is a cluster database')

    replication_system = models.ForeignKey(DatabaseReplicationSystem, on_delete=models.CASCADE, blank=True, null=True)

    def save(self, *args, **kwargs):
        self.database_type = OracleDatabase.CDB
        super().save(*args, **kwargs)

    def __str__(self):
        """Return string representation"""
        return f"{self.name} ({self.get_container_type_display()})"

    def get_absolute_url(self):
        """Return object absolute url"""
        if self.container_type == self.LEGACY:
            return reverse('database:database-oracle-leg-detail', kwargs={'pk': self.pk})
        else:
            return reverse('database:database-oracle-cdb-detail', kwargs={'pk': self.pk})

    class Meta:
        db_table = 'database_database_oracle_container'


class PluggableOracleDatabase(OracleDatabase):
    """Pluggable Oracle database"""

    guid = models.CharField('PDB GUID', max_length=32)
    container = models.ForeignKey(ContainerOracleDatabase, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.database_type = OracleDatabase.PDB
        super().save(*args, **kwargs)

    def __str__(self):
        """Return string representation"""
        return f"{self.name} (Pluggable Database)"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:database-oracle-pdb-detail', kwargs={'pk': self.pk})

    class Meta:
        db_table = 'database_database_oracle_pluggable'


class ContainerOracleFraMetric(Metric):
    """Database fra metric"""

    database = models.ForeignKey(OracleDatabase, on_delete=models.CASCADE)
    size = models.PositiveBigIntegerField('Size')
    used = models.PositiveBigIntegerField('Used size')
    reclaimable = models.PositiveBigIntegerField('Reclaimable size')

    class Meta(Metric.Meta):
        db_table = 'database_database_oracle_container_fra_metric'
