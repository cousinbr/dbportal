celery
django
djangorestframework
django_celery_results
django-csv-export-view
django-debug-toolbar
django-environ
django-mathfilters
grafana-django-saml2-auth
ldap3
tzdata